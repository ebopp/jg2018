# Overview

This is the code that generates the Website for the Japanese-German Meeting 2018
on Exoplanets and Planet Formation.

The main code is under `generator/`. The content is under `sections/` in the
form of Markdown documents and `data/` for the participant data. `static/`
contains static resources such as styles and images. HTML templates are under
`layouts/`. `secrets/` contains the encrypted password.


# Launch local server for testing

Requires a recent version of Python 3.x and the following packages:

- pystache
- flask
- frontmatter
- markdown2

If you are using Nix, you can simply use the `shell.nix` to setup the
environments.

Run this command to start a local webserver to preview your changes:

    python3 serve.py

The website will be served on `http://localhost:5000`.

Or (if you have entr and ripgrep to watch for changes):

    ./serve

Under `target/` you should find the static HTML, CSS and media as an output.


# Deploy to server via FTP

To deploy the artifact, you'll need:

- [lftp](https://lftp.tech/)
- [pass](https://www.passwordstore.org/)

The passwords are stored under secrets. Note: very likely the password will have
been reset after Eduard leaves at the end of November, so this needs to be
updated.
