{ pkgs, buildPythonPackage, pyyaml, six }:

buildPythonPackage rec {
  name = "frontmatter-${version}";
  version = "0.2.1";

  src = pkgs.fetchurl {
    url = "mirror://pypi/p/python-frontmatter/python-frontmatter-${version}.tar.gz";
    sha256 = "dd0cec2723fd974841226bd2d03e140224fe3b8f1ef48f1b42ee282939587eeb";
  };

  propagatedBuildInputs = [ pyyaml six ];

  doCheck = false;

  meta = {
    homepage = "https://python-frontmatter.readthedocs.io/en/latest/";
    description = "Parse and manage posts with YAML frontmatter";
    maintainers = [];
  };
}
