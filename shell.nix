with import <nixpkgs> {};

let python =
  let
    frontmatter = pkgs.callPackage ./packages/frontmatter.nix {
      pkgs = pkgs;
      inherit (pkgs.python36Packages) pyyaml six buildPythonPackage;
    };
  in pkgs.python36.withPackages (packages: with packages; [
    pystache
    flask
    frontmatter
    markdown2
  ]);

in stdenv.mkDerivation rec {
  name = "dejp-meeting-website";

  buildInputs = [
    python
    ripgrep
    entr
    lftp
  ];
}
