import csv

def participants(filename):
    with open(filename) as csvfile:
        reader = csv.reader(csvfile)
        def parse_row(row):
            return {
                "first_name": row[2].strip(),
                "last_name": row[1].strip(),
                "email": row[6].strip() if row[6].strip() != "" else row[7].strip(),
            }
        return sorted(
            list(map(parse_row, reader))[1:],
            key=lambda row: row["last_name"]
        )

things = participants("participants.csv")
print("Bcc:", ", ".join(
    "{first_name} {last_name} <{email}>".format(**row)
    for row in things
))
