----
title: Japanese-German meeting on Exoplanets and Planet Formation 2018
slug: welcome
order: 0

----

<div class="item">
    <h2>Date</h2>
    <p>September 24-28, 2018</p>
</div>

<div class="item">
    <a href="#location">
        <h2>Location</h2>
        <p>Edesheim, Germany</p>
    </a>
</div>

<div class="item register">
    <a href="#registration">
        <h2>Registration</h2>
        <p>invitation-only</p>
    </a>
</div>

<div class="item">
    <a href="#participants">
        <h2>Participants</h2>
        <p></p>
    </a>
</div>

<div class="item">
    <a href="#program">
        <h2>Program</h2>
        <p>5 days, 15 sessions</p>
    </a>
</div>

<div class="item">
    <h2>Social Activities</h2>
    <p>coming soon…</p>
</div>
