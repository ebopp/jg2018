----
title: Registration
slug: registration
order: 2

----

Please note, that at the moment the registration is only open after invitation!

We negotiated a price per participant at the Meeting in Edesheim of 770 €, which
will cover Hotel and Breakfast from Monday to Friday. Lunch is included Tuesday
to Thursday. The BBQ dinner on Monday is covered as well as hotel related
conference fees (coffee etc.).

<a href="https://docs.google.com/forms/d/e/1FAIpQLSfipyDPClkwSPC-9mjwabry72YgNpiyxJ1bYHMe04ZOgzw9RA/viewform" target="_blank">
→ to the registration form…
</a>
