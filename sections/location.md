----
title: Location
slug: location
order: 1

----

The meeting will take place at the [Hotel Schloss Edesheim][hotel] in the
southern part of Germany.

[hotel]: https://www.schloss-edesheim.de/en/


## Getting to Edesheim

Edesheim has a station called *Edesheim (Pfalz)* and has a train
service running roughly every 30 min.

Make sure to check the exact schedule ahead of time with [Deutsche Bahn][db],
the German railway operator.

[db]: https://www.bahn.com


## For international attendees

We recommend you to book your flights to Frankfurt airport (FRA). We have booked
a hotel from Sunday to Monday in Heidelberg at the Star Inn Hotel ([here's a
map][star-inn-hd]). It is very close to Heidelberg central station, so we
recommend you to take the train to Heidelberg and walk to the hotel.

[star-inn-hd]: https://goo.gl/maps/u4PiaiAcGDr

On Monday morning we will organise transport to Edesheim from Heidelberg. We
will meet in the lobby at the Star Inn at **8.20 am**. The transport will be
partially done by car, partially by train. Your luggage will be transported
separately.

For your return on Friday there are opportunities to transport you back to
Heidelberg or Mannheim from which there is regular train service to Frankfurt
airport.


## Finding the venue

If you arrive by train, you can walk to the conference venue in 10 minutes from
the station:


<div class="googlemap-responsive">
    <iframe src="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d2603.666452207258!2d8.133452316052114!3d49.263767679329284!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e2!4m5!1s0x47964fb83cc294d1%3A0xe92df30607ea3f3e!2sEdesheim(Pfalz)!3m2!1d49.2633064!2d8.1393233!4m5!1s0x47964fbf67ec0881%3A0x25887b2a98c8b74e!2sHotel+Schlo%C3%9F+Edesheim%2C+Privathotels+Dr.+Lohbeck+GmbH+%26+Co.+KG%2C+Luitpoldstra%C3%9Fe+9%2C+67483+Edesheim!3m2!1d49.2626881!2d8.132520699999999!5e0!3m2!1sen!2sde!4v1531747294662" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
