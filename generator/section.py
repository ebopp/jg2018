import os
from os.path import join
import re
import frontmatter
from .output import write
from .layout import layout

def load(directory):
    return sorted([
        Section(join(directory, filename))
        for filename in os.listdir(directory)
    ], key=lambda section: section.order)

class Section:
    def __init__(self, filename):
        self.filename = filename
        self._frontmatter = frontmatter.load(filename)
        self.content = self._frontmatter.content

    @property
    def link_name(self):
        match = re.match(".*\/(.*)\.md", self.filename)
        assert match
        return match.group(1)

    @property
    def order(self):
        return self._frontmatter["order"]

    def build(self):
        style = self._frontmatter.get("style", "section")
        participants = load_participants()
        if self._frontmatter.get("build_schedule", False):
            schedule = extract_schedule(participants, self.content)
            attrs = {"schedule": schedule, **self._frontmatter}
        elif self._frontmatter.get("slug") == "participants":
            attrs = {"participants": participants, **self._frontmatter}
        else:
            attrs = self._frontmatter
        return layout(style, self.content, attrs)

def load_participants():
    import csv
    with open("data/participants.csv") as csvfile:
        reader = csv.reader(csvfile)
        def parse_row(row):
            return {
                "last_name": row[1],
                "first_name": row[2],
                "length": row[3],
                "title": row[4],
                "abstract": row[5],
                "affiliation": row[9],
            }
        return sorted(
            list(map(parse_row, reader))[1:],
            key=lambda row: row["last_name"]
        )

def extract_schedule(participants, content):
    schedule = []
    for line in content.split("\n"):
        if line.strip() == "":
            continue
        if ":" in line:
            time, description = line.split(":", 1)
            item = {"time": time.strip()}
            if description.strip().startswith("Session"):
                item["session"] = extract_session(participants, description)
            else:
                item["description"] = description
            schedule[-1]["items"].append(item)
        else:
            schedule.append({"day": line.strip(), "items": []})
    return schedule

def extract_session(participants, text):
    raw_title, raw_speakers = text.split(":")
    title = raw_title.strip()
    speakers = [
        lookup_participant_by_last_name(participants, last_name.strip())
        for last_name in raw_speakers.split(",")
    ]
    return {"title": title, "speakers": speakers}

def lookup_participant_by_last_name(participants, last_name):
    for participant in participants:
        if participant["last_name"] == last_name:
            return participant
