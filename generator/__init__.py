import os
import os.path
import shutil
from pystache import render
from .output import write, clean_up, copy_static_files
from .layout import layout
from . import section, index

def build():
    clean_up()
    copy_static_files()
    render_site()

def render_site():
    write("/", index.render(section.load("sections")))

if __name__ == "__main__":
    build()
