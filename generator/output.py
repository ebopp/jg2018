import os
import shutil

target = "target"
static = "static"

def copy_static_files():
    for path in os.listdir(static):
        shutil.copytree(os.path.join(static, path), os.path.join(target, path))

def clean_up():
    shutil.rmtree(target, ignore_errors=True)
    os.makedirs(target)

def write(path, content):
    assert path[0] == "/", "path must start at root"
    directory = os.path.join(target, path[1:])
    os.makedirs(directory, exist_ok=True)
    with open(os.path.join(directory, "index.html"), "w") as html:
        html.write(content)
