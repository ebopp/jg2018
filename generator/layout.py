from pystache import render
import markdown2
from os.path import join

_layout_folder = "layouts/"

def layout(style, content, attributes=None):
    attributes = attributes or {}
    content = render(content, **attributes)
    return render(layout_file(style), { "content": markdown(content), **attributes })

def layout_file(name):
    return open(join(_layout_folder, name + ".html")).read()

def markdown(content):
    return markdown2.markdown(content)
