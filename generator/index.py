from .layout import layout

def render(sections):
    content = "\n".join(section.build() for section in sections)
    return layout("index", content)
