import generator
import os.path
from flask import Flask, send_from_directory, send_file

generator.build()

app = Flask('static file server')

@app.route('/')
def root():
    return send_file(generator.output.target + '/index.html')

@app.route('/<path:path>')
def other_paths(path):
    if os.path.isdir(os.path.join(generator.output.target, path)):
        path = path + '/index.html'
    return send_from_directory(generator.output.target, path)

app.run()
